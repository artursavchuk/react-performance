import { createSlice } from '@reduxjs/toolkit'
import { PayloadAction } from '@reduxjs/toolkit'
import { Character, Starship } from '../helper/localData'

type InitialState = {
  character: Character | null
  starship: Starship | null
  test: boolean
}

const initialState: InitialState = {
  character: null,
  starship: null,
  test: false,
}

export const swSlice = createSlice({
  name: 'sw',
  initialState,
  reducers: {
    setCharacter: (state, action: PayloadAction<Character>) => {
      state.character = action.payload
    },
    setStarship: (state, action: PayloadAction<Starship>) => {
      state.starship = action.payload
    },
    setTest: (state) => {
      state.test = true
    },
  },
})

export const { reducer: swReducer, actions: swActions } = swSlice
