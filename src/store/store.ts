import { configureStore } from '@reduxjs/toolkit'
import { swReducer } from './swSlice'
import { swApi } from '../api/swSlice'

export const store = configureStore({
  reducer: {
    sw: swReducer,
    [swApi.reducerPath]: swApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(swApi.middleware),
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
