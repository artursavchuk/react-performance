import './style.scss'
import { store } from './store/store'
import { Provider } from 'react-redux'
import { ObjectProp } from './sections/ObjectProp'
import { MappingList } from './sections/MappingList'
import { UsingStore } from './sections/UsingStore'
import { LazyWithSuspense } from './sections/LazyWithSuspense'
import { RTKQuery } from './sections/RTKQuery'
import { ForcedReflow } from './sections/ForcedReflow'

export const App = () => {
  return (
    <Provider store={store}>
      <div className="app">
        <div className="comet"></div>
        <ObjectProp />
        <MappingList />
        <UsingStore />
        <LazyWithSuspense />
        <RTKQuery />
        <ForcedReflow />
      </div>
    </Provider>
  )
}
