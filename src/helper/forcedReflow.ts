const getRandomText = () => {
  return Math.random().toString(36).slice(2)
}

export const deleteChildren = (ref: HTMLDivElement) => {
  ref.textContent = ''
}

export const appendChildren = (ref: HTMLDivElement, count: number) => {
  for (let i = 0; i < count; i++) {
    const div = document.createElement('div')
    div.textContent = getRandomText()
    ref.appendChild(div)
  }
}

export const isDeathStar = (hero: HTMLImageElement) => {
  return hero.classList.contains('starship')
}

export const isYoda = (hero: HTMLImageElement) => {
  return hero.classList.contains('yoda')
}
