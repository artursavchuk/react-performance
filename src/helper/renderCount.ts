export const renderCount: Record<string, number> = {}

export const increaseRenderCount = (title: string) => {
  const count = renderCount[title]
  renderCount[title] = count ? count + 1 : 1
}
