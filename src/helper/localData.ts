export const characters = [
  'Darth Vader',
  'R2-D2',
  'Yoda',
  'Luke Skywalker',
] as const

export const starships = [
  'Executor',
  'Death Star',
  'Millennium Falcon',
  'X-wing',
] as const

export type Character = (typeof characters)[number]
export type Starship = (typeof starships)[number]

type SingleTab = {
  image: string
  text: string
}
export type TabsData = Record<1 | 2 | 3, SingleTab>

export const tabsData: TabsData = {
  1: {
    image: '/5.jpg',
    text: 'Sentinel-class landing craft',
  },
  2: {
    image: '/9.jpg',
    text: 'Death Star',
  },
  3: {
    image: '/15.jpg',
    text: 'Executor',
  },
}
