import { memo, useCallback, useMemo } from 'react'
import { renderCount, increaseRenderCount } from '../helper/renderCount'

export type PersonType = { name: string }

type Props = {
  person: PersonType
  title: string
  handleClick?: () => void
}

// Simple component
export const Person = ({ person, title, handleClick }: Props) => {
  increaseRenderCount(title)

  return (
    <div className="card" onClick={handleClick}>
      <p>{title}</p>
      <p className="text">Name: {person.name}</p>
      <p>Number of renders: {renderCount[title]}</p>
    </div>
  )
}

// Memoized Component
export const MemoPerson = memo(Person)

// Item of list as memoized component
export const MemoListItemPerson = ({ person, title, handleClick }: Props) => {
  const memoizedPerson = useMemo(() => ({ name: person.name }), [person.name])
  const memoizedHandleClick = useCallback(
    () => handleClick?.(),
    [memoizedPerson]
  )
  return (
    <MemoPerson
      person={memoizedPerson}
      title={title}
      handleClick={memoizedHandleClick}
    />
  )
}
