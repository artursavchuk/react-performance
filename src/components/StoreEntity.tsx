import { renderCount, increaseRenderCount } from '../helper/renderCount'
import { useAppSelector } from '../hooks/useStore'

const notOptimized = 'Not optimized'
const optimized = 'Optimized'

export const NotOptimizedUsingStore = () => {
  increaseRenderCount(notOptimized)
  const sw = useAppSelector((store) => store.sw)
  return (
    <div className="card">
      <p>{notOptimized}</p>
      <p className="text">Character: {sw.character ?? 'not selected'}</p>
      <p>Number of renders: {renderCount[notOptimized]}</p>
    </div>
  )
}

export const OptimizedUsingStore = () => {
  increaseRenderCount(optimized)
  const value = useAppSelector((store) => store.sw.starship)

  return (
    <div className="card">
      <p>{optimized}</p>
      <p className="text">Starship: {value ?? 'not selected'}</p>
      <p>Number of renders: {renderCount[optimized]}</p>
    </div>
  )
}
