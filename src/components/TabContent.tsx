import { memo } from 'react'
import { TabsData, tabsData } from '../helper/localData'

type Props = {
  tabIndex: keyof TabsData
}

const TabContent = memo(({ tabIndex }: Props) => {
  const { image, text } = tabsData[tabIndex]

  return (
    <div>
      <img src={image} />
      <p>{text}</p>
    </div>
  )
})

export default TabContent
