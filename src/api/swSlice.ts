import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

type StarshipResponse = {
  name: string
  url: string
}

export const swApi = createApi({
  reducerPath: 'swApi',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://swapi.dev/api/' }),
  endpoints: (builder) => ({
    getStarshipsList: builder.query<StarshipResponse[], void>({
      query: () => 'starships/',
      transformResponse: ({ results }) => results,
    }),

    getStarship: builder.query<StarshipResponse, number>({
      query: (id) => `starships/${id}`,
    }),
  }),
})

export const { useGetStarshipsListQuery, useLazyGetStarshipQuery } = swApi
