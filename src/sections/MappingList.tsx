import { useState } from 'react'
import { MemoListItemPerson, Person, PersonType } from '../components/Person'
import { characters } from '../helper/localData'

export const MappingList = () => {
  const [character, setCharacter] = useState<PersonType | null>(null)
  const [character2, setCharacter2] = useState<PersonType | null>(null)
  return (
    <section className="section">
      <h2>Optimizing the rendering of a list that includes event handling</h2>

      <div className="card">
        <h3>Not optimized rendering of a list</h3>
        <h3>Select a character, then click on it again</h3>
        <div className="cards-wrapper">
          {characters.map((name, i) => {
            const person = { name }
            return (
              <Person
                key={name}
                title={`<Person ${++i} /> + simple object`}
                person={person}
                handleClick={() => setCharacter(person)}
              />
            )
          })}
        </div>

        <h3>Favorite character: {character?.name ?? 'not selected'}</h3>
      </div>

      <div className="card mt">
        <h3>Optimized rendering of a list</h3>
        <h3>Select a character, then click on it again</h3>

        <div className="cards-wrapper">
          {characters.map((name, i) => {
            const person = { name }
            return (
              <MemoListItemPerson
                key={name}
                title={`<MemoPerson ${++i} /> + memoized object`}
                person={person}
                handleClick={() => setCharacter2(person)}
              />
            )
          })}
        </div>

        <h3>Favorite character: {character2?.name ?? 'not selected'}</h3>
      </div>
    </section>
  )
}
