import { Suspense, lazy, useState } from 'react'
import { TabsData } from '../helper/localData'
const TabContent = lazy(() => delayForDemo(import('../components/TabContent')))

export const LazyWithSuspense = () => {
  const [tab, setTab] = useState<keyof TabsData | null>(null)

  return (
    <div className="section">
      <h2>Using Lazy from React + Suspense</h2>

      <div className="cards-wrapper">
        <div className="card">
          <div className="tabs">
            <button onClick={() => setTab(1)}>Tab 1</button>
            <button onClick={() => setTab(2)}>Tab 2</button>
            <button onClick={() => setTab(3)}>Tab 3</button>
          </div>
          <div className="content">
            <Suspense fallback={'Loading'}>
              {!tab &&
                'Select some tab to loading component by Lazy from React'}
              {tab && <TabContent tabIndex={tab} />}
            </Suspense>
          </div>
        </div>
      </div>
    </div>
  )
}

function delayForDemo(promise) {
  return new Promise((resolve) => {
    setTimeout(resolve, 2000)
  }).then(() => promise)
}
