import { useRef, useState } from 'react'
import {
  appendChildren,
  deleteChildren,
  isDeathStar,
  isYoda,
} from '../helper/forcedReflow'

const oneRenderCount = 10_000

export const ForcedReflow = () => {
  const [clickCount, setClickCount] = useState(0)
  const [isMovedYoda, setIsMovedYoda] = useState(false)
  const inner = useRef<HTMLDivElement>(null)
  const deathStar = useRef<HTMLImageElement>(null)
  const yoda = useRef<HTMLImageElement>(null)

  const handleClick = () => {
    if (!inner.current) return
    appendChildren(inner.current, oneRenderCount)
    setClickCount((prev) => prev + 1)
  }

  const handleDelete = () => {
    if (!inner.current) return
    deleteChildren(inner.current)
    setClickCount(0)
  }

  const moveYoda = (hero: HTMLImageElement, inner: HTMLDivElement) => {
    const heroWidth = hero.offsetWidth
    const transformPosition = inner.offsetWidth - heroWidth
    const newTranslate = isMovedYoda ? 0 : transformPosition
    hero.style.transform = `translateX(${newTranslate}px)`
    setIsMovedYoda((prev) => !prev)
  }

  const moveDeathStar = (hero: HTMLImageElement, inner: HTMLDivElement) => {
    const heroWidth = hero.offsetWidth
    const currentLeft = parseInt(hero.style.left, 10)
    const rightPosition = inner.offsetWidth - heroWidth - 5
    const newLeft = currentLeft === rightPosition ? 5 : rightPosition
    hero.style.left = `${newLeft}px`
  }

  const handleMove = (heros: (HTMLImageElement | null)[]) => {
    if (!heros.length) return

    heros.forEach((hero) => {
      if (!inner.current || !hero) return

      if (isDeathStar(hero)) {
        moveDeathStar(hero, inner.current)
      }

      if (isYoda(hero)) {
        moveYoda(hero, inner.current)
      }
    })
  }

  return (
    <section className="section">
      <h2>Forced reflow</h2>
      <h3>Don't look in Firefox, the developers did their best there</h3>
      <div className="buttons">
        <button onClick={handleClick}>append {oneRenderCount} elements</button>
        <button onClick={handleDelete}>delete elements</button>
        <button onClick={() => handleMove([deathStar.current])}>
          move death star
        </button>
        <button onClick={() => handleMove([yoda.current])}>move yoda</button>
        <button onClick={() => handleMove([deathStar.current, yoda.current])}>
          move both
        </button>
      </div>

      {!!clickCount && (
        <p className="mt">Rendered: {clickCount * oneRenderCount} elements</p>
      )}

      <div className="card mt forced-reflow">
        <div ref={inner} className="grid"></div>
        <img
          src="/death-star.svg"
          alt="death-star"
          className="starship"
          ref={deathStar}
        />
        <img src="/public/yoda.webp" alt="yoda" className="yoda" ref={yoda} />
      </div>
    </section>
  )
}
