import { useMemo, useState } from 'react'
import { MemoPerson, Person } from '../components/Person'

export const ObjectProp = () => {
  const [person, setPerson] = useState({ name: 'Darth Vader' })
  const memoizedPerson = useMemo(() => ({ name: person.name }), [person.name])

  return (
    <section className="section">
      <h2>Memoization of components that have a prop type equal to 'object'</h2>

      <div className="cards-wrapper">
        <Person person={person} title="<Person />" />
        <MemoPerson person={person} title="<MemoPerson /> + simple object " />
        <MemoPerson
          person={memoizedPerson}
          title="<MemoPerson /> + memoized object"
        />
      </div>
      <button onClick={() => setPerson({ name: 'Darth Vader' })}>
        update state, <br />
        but set same value
      </button>
    </section>
  )
}
