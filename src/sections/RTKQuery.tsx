import {
  useGetStarshipsListQuery,
  useLazyGetStarshipQuery,
} from '../api/swSlice'

export const RTKQuery = () => {
  const { data: starships, isLoading } = useGetStarshipsListQuery()
  const [loadStarship, { isFetching, data: currentStarship }] =
    useLazyGetStarshipQuery()

  const handleClick = (url: string) => {
    const match = url.match(/\/(\d+)\/$/)
    const id: number | null = match ? +match[1] : null
    if (!id) return
    loadStarship(id, true)
  }

  return (
    <div className="section">
      <h2>Using RTK Query</h2>
      {isLoading && <p>Loading</p>}

      <div className="cards-wrapper">
        {!isLoading && starships && (
          <div className="card">
            <ul className="list">
              {starships.map((s) => (
                <li key={s.name} onClick={() => handleClick(s.url)}>
                  {s.name}
                </li>
              ))}
            </ul>
          </div>
        )}

        {isFetching && <p>Fetching</p>}
        {!isFetching && currentStarship && (
          <div className="card">{currentStarship.name}</div>
        )}
      </div>
    </div>
  )
}
