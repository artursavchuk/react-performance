import {
  NotOptimizedUsingStore,
  OptimizedUsingStore,
} from '../components/StoreEntity'
import { characters, starships } from '../helper/localData'
import { useAppDispatch } from '../hooks/useStore'
import { swActions } from '../store/swSlice'

export const UsingStore = () => {
  const dispatch = useAppDispatch()

  const handleSetRandomCharacter = () => {
    const random = Math.floor(Math.random() * characters.length)
    dispatch(swActions.setCharacter(characters[random]))
  }
  const handleSetRandomStarship = () => {
    const random = Math.floor(Math.random() * starships.length)
    dispatch(swActions.setStarship(starships[random]))
  }

  return (
    <section className="section">
      <h2>
        Using a global store without fetching redundant information for the
        component
      </h2>

      <div className="cards-wrapper">
        <div className="card">
          <button onClick={handleSetRandomCharacter}>
            set random character
          </button>
          <button onClick={handleSetRandomStarship}>set random starship</button>
        </div>

        <div className="card">
          <NotOptimizedUsingStore />
          <OptimizedUsingStore />
        </div>
      </div>
    </section>
  )
}
